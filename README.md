# haz

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run regression tests
```
npm run test
```

### See docs for code review changes and requirements document

## ChangeLog

### Front End
```
Features implemented:
Report Table (2 kinds, waiting on feeback from client) Data Table, List format
Alert Table (shows alerts that exist in the database)
Alert Notification table (shows number of alerts based on hazard type/category) (Only supports Fire, Flood, and Shooting atm)
Map (Shows reports based on lng and lat on a google map)
    - Implemented click connection to report tables and selecting a pin will highlight the report on the right and vice versa
    - Implemented geofence and ability to create polygons on map. Saved locally atm until proper structure implemented in database
Alert Form Modal (Allows for creation of new alerts with ability to designate message, group to send to, and hazard type)
Updates to vuex's index.js
    - Implemented fetch functions for reports, groups, hazards, events, alerts from database
    - Implemented insert function for alerts to database

```

### Back End
```
Added time column to reports
Added some dummy data to hazard and events tables
```

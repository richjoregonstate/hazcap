import Vue from "vue";
import Vuex from "vuex";
import { apolloProvider } from "../plugins/apollo";
import gql from "graphql-tag";

Vue.use(Vuex);

//Todo Queries
const todoQuery = gql`
  {
    todos(order_by: { created_at: asc }) {
      id
      is_completed
      text
      created_at
    }
  }
`;

//Report Queries
const reportQuery = gql`
  {
    report(order_by: { id: asc }) {
      id
      hazard
      description
      lat
      long
      time
      uid
    }
  }
`;

//User Queries
const userQuery = gql`
  {
    users {
      name
      id
    }
  }
`;

//Alert Queries
const alertQuery = gql`
  {
    alert(order_by: { id: asc }) {
      id
      description
      gid
      eid
    }
  }
`;

//Mutation for adding an alert. Getting a typerror in promise of not being able to read affected rows but won't work without it
const alertMutation = gql`
  mutation insert_alerts($text: String!, $eid: Int, $gid: Int) {
    insert_alert(objects: { description: $text, eid: $eid, gid: $gid }) {
      affected_rows
    }
  }
`;

//Event Queries
const eventQuery = gql`
  {
    event(order_by: { id: asc }) {
      id
      hid
      active
      title
    }
  }
`;

//Hazard Queries
const hazardQuery = gql`
  {
    hazard(order_by: { id: asc }) {
      id
      instructions
      name
      protocols
    }
  }
`;

//Group Queries
const groupQuery = gql`
  {
    group(order_by: { id: asc }) {
      id
      name
      description
    }
  }
`;

const state = {
  // todos: JSON.parse(window.localStorage.getItem(STORAGE_KEY) || "[]")
  todos: [],
  reports: [],
  users: [],
  alerts: [],
  events: [],
  hazards: [],
  groups: []
};

const mutations = {
  //Todos
  fetchTodos(state, todos) {
    state.todos = todos;
  },
  //Reports
  fetchReports(state, reports) {
    state.reports = reports;
  },
  //Users
  fetchUsers(state, users) {
    state.users = users;
  },
  //Alerts
  fetchAlerts(state, alerts) {
    state.alerts = alerts;
  },
  addAlert(state, alert) {
    state.alerts.push(alert);
  },
  //Events
  fetchEvents(state, events) {
    state.events = events;
  },
  //Hazards
  fetchHazards(state, hazards) {
    state.hazards = hazards;
  },
  //Groups
  fetchGroups(state, groups) {
    state.groups = groups;
  }
};

const actions = {
  //Todos
  async fetchTodos({ commit }) {
    const { data } = await apolloProvider.defaultClient.query({
      query: todoQuery
    });
    commit("fetchTodos", data.todos);
  },
  //Reports
  async fetchReports({ commit }) {
    const { data } = await apolloProvider.defaultClient.query({
      query: reportQuery
    });
    commit("fetchReports", data.report);
  },
  //Users
  async fetchUsers({ commit }) {
    const { data } = await apolloProvider.defaultClient.query({
      query: userQuery
    });
    commit("fetchUsers", data.users);
  },
  //Alerts
  async fetchAlerts({ commit }) {
    const { data } = await apolloProvider.defaultClient.query({
      query: alertQuery
    });
    commit("fetchAlerts", data.alert);
  },
  async addAlert({ commit }, text) {
    // console.log("Data!")
    const { data } = await apolloProvider.defaultClient.mutate({
      mutation: alertMutation,
      variables: { text: text.text, eid: text.eid, gid: text.gid }
    });
    console.log(data);
    if (data.insert_alerts.affected_rows) {
      commit("addAlert", data.insert_alerts.returning[0]);
    }
  },
  //Events
  async fetchEvents({ commit }) {
    const { data } = await apolloProvider.defaultClient.query({
      query: eventQuery
    });
    commit("fetchEvents", data.event);
  },
  //Hazards
  async fetchHazards({ commit }) {
    const { data } = await apolloProvider.defaultClient.query({
      query: hazardQuery
    });
    commit("fetchHazards", data.hazard);
  },
  //Groups
  async fetchGroups({ commit }) {
    const { data } = await apolloProvider.defaultClient.query({
      query: groupQuery
    });
    commit("fetchGroups", data.group);
  }
};

const modules = {};

export default new Vuex.Store({
  state,
  mutations,
  actions,
  modules
});

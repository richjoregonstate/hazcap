import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import vueApollo from "vue-apollo";
import ApolloProvider from "./plugins/apollo";
import { domain, clientId } from "./plugins/auth/auth_config";
import { Auth0Plugin } from "./plugins/auth/auth";
import "@babel/polyfill";
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(vueApollo);
Vue.use(Auth0Plugin, {
  domain,
  clientId,
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyBHKQHB3WXPUS7z7bCnlNHhA-isVof3sSk",
    libraries: "places" // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    //// If you want to set the version, you can do so:
    // v: '3.26',
  },

  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,

  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then set installComponents to 'false'.
  //// If you want to automatically install all the components this property must be set to 'true':
  installComponents: true
});

Vue.config.productionTip = false;

new Vue({
  router,
  ApolloProvider,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");

These files are for show. The docker-compose file has been scrubbed off all critical info.

Anything that has been removed has ... in it's place. All but HASURA_GRAPHQL_JWT_SECRET are simple passwords. Ergo you could put in your own and it would work.

All that being said even if you set up the docker-compose you wouldn't be able to connect to your local instance until you edit the apollo
settings

/*
HOW TO USE:
Grab chromedriver for your chrome version here: https://chromedriver.chromium.org/downloads
put the chromedriver.exe in the root directory
run: npm run test
*/
const { Builder, By, Key, until } = require("selenium-webdriver");

//Test logging in
(async function login() {
  //Grab a window
  let driver = await new Builder().forBrowser("chrome").build();

  //Login test
  try {
    await driver.get("http://localhost:8080");
    await driver.wait(until.titleIs("haz"), 1000);
    await driver.findElement(By.xpath(`//*[@id="app"]/div/button`)).click();
    await driver.wait(until.titleIs("Sign In with Auth0"), 1000);
    await driver.executeAsyncScript(
      "window.setTimeout(arguments[arguments.length - 1], 3000);"
    );
    await driver
      .findElement(By.id("1-email"))
      .sendKeys("zegfavxhqjhuzunrfi@awdrt.org");
    await driver
      .findElement(By.name("password"))
      .sendKeys("47qf6sv3gQ5Rm4m", Key.RETURN);
    await driver.wait(until.titleIs("haz"), 1000);
  } catch (err) {
    console.log(err);
  } finally {
    console.log("✔ Logged in successfully");
  }

  //Everything else is Hasura witch has it's own tests.
})();
